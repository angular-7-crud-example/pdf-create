package com.hendisantika.pdfcreate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PdfCreateApplication {

    public static void main(String[] args) {
        SpringApplication.run(PdfCreateApplication.class, args);
    }

}
